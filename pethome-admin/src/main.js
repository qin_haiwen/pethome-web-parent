import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//import './assets/theme/theme-green/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
//import NProgress from 'nprogress'
//import 'nprogress/nprogress.css'
import routes from './routes'
/*import Mock from './mock'
Mock.bootstrap();*/
import axios from 'axios'
//配置axios的全局基本路径
axios.defaults.baseURL='http://localhost:8080/'
//全局属性配置，在任意组件内可以使用this.$http获取axios对象
Vue.prototype.$http = axios
import 'font-awesome/css/font-awesome.min.css'

Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//NProgress.configure({ showSpinner: false });

const router = new VueRouter({
    routes
})

/*router.beforeEach((to, from, next) => {
  //NProgress.start();
  if (to.path == '/login') {
    sessionStorage.removeItem('user');
  }
  let user = JSON.parse(sessionStorage.getItem('user'));
  if (!user && to.path != '/login') {
    next({ path: '/login' })
  } else {
    next()
  }
})*/

//router.afterEach(transition => {
//NProgress.done();
//});
//  静态资源拦截器
router.beforeEach((to, from, next) => {

    if (to.path == '/login' || to.path == '/shopRegister') {
        localStorage.removeItem('token');
        localStorage.removeItem('logininfo');
        next()
        return;
    }
    let logininfo = JSON.parse(localStorage.getItem('logininfo'));
    if (!logininfo) {
        next({ path: '/login'})
    } else {
        next()
    }
})

// 前置拦截器
axios.interceptors.request.use(config=>{
    //携带token
    let token =  localStorage.getItem("token");
    if(token){
        config.headers['token']=token;
    }
    return config;
},error => {
    Promise.reject(error);
})

// 后置拦截器  {success: false, message: "noLogin"}
axios.interceptors.response.use(config=>{

    let data = config.data;
    if(!data.success && "noLogin"===data.message)
    {
        localStorage.removeItem("token");
        localStorage.removeItem("logininfo");
        router.push({ path: '/login' });
    }
    return config;
},error => {
    Promise.reject(error)
})

new Vue({
    //el: '#app',
    //template: '<App/>',
    router,
    store,
    //components: { App }
    render: h => h(App)
}).$mount('#app')