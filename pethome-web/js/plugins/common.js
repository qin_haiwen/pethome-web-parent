//配置axios的全局基本路径
axios.defaults.baseURL='http://localhost:8080/'
//全局属性配置，在任意组件内可以使用this.$http获取axios对象
Vue.prototype.$http = axios

// 静态资源拦截器
// 获取访问的url
let url = location.href;
if( url.indexOf("index.html")===-1
    && url.indexOf("register.html")===-1
    && url.indexOf("login.html")===-1
    && url.indexOf("callback.html")===-1
){
    // 有没有登录
    let logininfo = localStorage.getItem("logininfo")
    if(logininfo==null){
        localStorage.removeItem("token");
        // 跳转
        location.href = "login.html"
    }
}

// 前置拦截器
axios.interceptors.request.use(config=>{
    //携带token
    let token =  localStorage.getItem("token");
    if(token){
        config.headers['token']=token;
    }
    return config;
},error => {
    Promise.reject(error);
})

// 后置拦截器  {success: false, message: "noLogin"}
axios.interceptors.response.use(config=>{

    let data = config.data;
    if(!data.success && "noLogin"===data.message)
    {
        localStorage.removeItem("token");
        localStorage.removeItem("logininfo");

        //  跳转页面
        location.href="login.html"

    }
    return config;
},error => {
    Promise.reject(error)
})

/**
 * 解析url获取参数对象
 * @param url
 * @returns {{}}
 */
function parseUrlParams2Obj(url) {
    //获取请求参数
    let paramStr = url.substring(url.indexOf("?") + 1);//code=021FdXFa14mYlA016OGa1L3R4X0FdXFO&state=1
    let paramStrs = paramStr.split("&");//[code=021FdXFa14mYlA016OGa1L3R4X0FdXFO,state=1]
    let paramObj = {};
    for (let i = 0; i < paramStrs.length; i++) {
        let paramName = paramStrs[i].split("=")[0];//code
        let paramValue = paramStrs[i].split("=")[1];//021FdXFa14mYlA016OGa1L3R4X0FdXFO
        paramObj[paramName] = paramValue;
    }
    return paramObj;
}